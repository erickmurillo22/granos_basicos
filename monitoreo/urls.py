from django.urls import path, include
from .views import *

urlpatterns = [
	path('productores/genero-y-produccion/', genero_produccion, name='genero-y-produccion'),
    path('productores/composicion-familiar/', composicion_familiar, name='composicion-familiar'),

    path('monitoreo/georeferencia/', georeferencia, name='georeferencia'),
    path('monitoreo/caracteristicas-parcela/', caracteristicas_parcela, name='caracteristicas-parcela'),
    path('monitoreo/ciclo-productivo/', ciclo_productivo, name='ciclo-productivo'),
    path('monitoreo/uso-suelo/', uso_suelo, name='uso-suelo'),
    path('monitoreo/recursos-economicos/', recursos_economicos, name='recursos-economicos'),
    path('monitoreo/rendimiento/', rendimiento, name='rendimiento'),

    path('ajax/comunies/', get_comunies, name='get-comunies'),
]