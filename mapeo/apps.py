from django.apps import AppConfig


class MapeoConfig(AppConfig):
    name = 'mapeo'
